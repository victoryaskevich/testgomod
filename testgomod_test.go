package testgomod

import "testing"
import "gitlab.com/BulbaBelorus/testgomod/somepack"


func TestConc(t *testing.T) {
	var want = "Hey" + " guys!"
	var have = somepack.Conc("Hey", " guys!")
	if have != want { t.Error("Have != want") }
}